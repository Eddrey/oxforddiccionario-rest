
<%@page import="java.util.Iterator"%>
<%@page import="root.model.entities.Diccionario"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%
    List<Diccionario> listapalabra = (List<Diccionario>) request.getAttribute("listapalabra");
    Iterator<Diccionario> itListapalabra = listapalabra.iterator();
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            html, body {
                margin:0;
                padding:0;
                height:50%;
            }
            footer{
                height:50px;
                line-height:50px;
                background:black;
                color:white;
                text-align:center;
                position:fixed;
                bottom:0;
                left:0;
                width:100%
            } 
            table{
                border: 1px solid #000;
                border-collapse: separate;
                margin: 250px;
                margin-top: auto;
                width: 900px;               
            }
            table td{
                border: 1px solid #000;                
                padding: 20px;
            }
            table th{
                border: 1px solid #000;
                padding: 10px;
            }
            h1{
                margin: 50px 250px;
            }
            a{
                margin: 50px 250px;
                margin-top: 10px;
            }
        </style>
        <title>Historial</title>
    </head>
    <body>
        <h1>Historial de palabras</h1>

        <table>
            <thead>
            <th>ID</th>
            <th>Palabra</th>
            <th>Significado</th>                    
        </thead>
        <tbody>
            <%while (itListapalabra.hasNext()) {
                    Diccionario historial = itListapalabra.next();%>
            <tr>
                <td><%=historial.getDicId()%>
                <td><%=historial.getDicPalabra()%></td>
                <td><%=historial.getDicSignificado()%></td> 
            </tr>
            <%}%>

        </tbody>
    </table>
    <a href="index.jsp">Buscar otra palabra</a>
    <!--<a href="/">Buscar otra palabra</a>-->
    <footer>
        <span>Taller de Aplicaciones Empresariales - Evaluación Final *** Eddrey Malebranche ***-<a href="https://bitbucket.org/Eddrey/oxforddiccionario-rest/src"> Enlace a Bitbucket </a></span>
    </footer> 
</body>
</html>
