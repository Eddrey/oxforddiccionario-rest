
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>  
            html, body {
                margin:0;
                padding:0;
                height:50%;
            }
            footer{
                height:50px;
                line-height:50px;
                background:black;
                color:white;
                text-align:center;
                position:fixed;
                bottom:0;
                left:0;
                width:100%;
            }                       
            
            #titulo{
                margin: 50px 250px;
                width: 900px; 
                text-align: center;
            }
            #form{
                
            }           
        </style>
        <title>Api Oxford</title>
    </head>
    <body>
        <div>
            <div id="titulo">
                <p><h1>DICCIONARIO DE OXFORD</h1></p>
                <p><h1>************************************</h1></p>
            </div>
            <div id="form">            
                <p>    
                <form action="controller" method="POST">
                    <input  type="text" placeholder="Palabra a buscar" name="palabra" required>
                    <button type="submit" name="accion" value="buscar">Buscar</button>
                </form>
                </p>
            </div>  

            <form name="historial" action="controller" method="GET">
                <input class="historial" type="submit" value="Historial" />
            </form>
            <footer>
                <span>Taller de Aplicaciones Empresariales - Evaluación Final *** Eddrey Malebranche ***-<a href="https://bitbucket.org/Eddrey/oxforddiccionario-rest/src"> Enlace a Bitbucket </a></span>
            </footer>                    
        </div>
    </body>
</html>
