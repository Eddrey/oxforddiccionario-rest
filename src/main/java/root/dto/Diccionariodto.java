
package root.dto;

import java.io.Serializable;

public class Diccionariodto implements Serializable{
    
    private String dic_palabra;
    private String dic_significado;

    public Diccionariodto() {
    }

    public String getDic_palabra() {
        return dic_palabra;
    }

    public void setDic_palabra(String dic_palabra) {
        this.dic_palabra = dic_palabra;
    }

    public String getDic_significado() {
        return dic_significado;
    }

    public void setDic_significado(String dic_significado) {
        this.dic_significado = dic_significado;
    }
    
    
  
}
