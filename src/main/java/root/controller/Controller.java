package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import root.dto.Diccionariodto;
import root.model.dao.DiccionarioDAO;
import root.model.entities.Diccionario;

@WebServlet(name = "Controller", urlPatterns = {"/controller"})
public class Controller extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Controller</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Controller at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
        DiccionarioDAO dao = new DiccionarioDAO();
        List<Diccionario> listapalabra = dao.findDiccionarioEntities();
        request.setAttribute("listapalabra", listapalabra);
        

        request.getRequestDispatcher("historial.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        /*
     try {   
            Diccionariodto pal = new Diccionariodto();

            System.out.print("doPost:" );

            String palabra = request.getParameter("palabra");

            //recuperar la palabra
            //servicio rest dicccionar local
            /*
            Client client = ClientBuilder.newClient();
            WebTarget myResource = client.target("http://DESKTOP-802N53F:8080/oxforddiccionario-rest-1.0-SNAPSHOT/api/oxford/" + palabra);

            pal = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "KEYSEC0001918").header("api-id", "111111").get(Diccionariodto.class);
         */

 /*
            Diccionario palabraBD = new Diccionario();
            palabraBD.setDicPalabra(palabra);
            palabraBD.setDicSignificado("Significado de la palabra");
            //palabraBD.setDicPalabra(pal.getDic_palabra());
            //palabraBD.setDicSignificado(pal.getDic_significado());

            DiccionarioDAO dao = new DiccionarioDAO();

            dao.create(palabraBD);
            
            List<Diccionario> listapalabra = dao.findDiccionarioEntities();
            resquest.setAttribute("listapalabra")

            request.getRequestDispatcher("index.jsp").forward(request,response);   
            
        } catch (Exception ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        /*
        DiccionarioDAO dao = new DiccionarioDAO();
        List<Diccionario> listapalabra = dao.findDiccionarioEntities();
        request.setAttribute("listapalabra", listapalabra);
         */
 
        System.out.println("doPost:");
        String palabra = request.getParameter("palabra");

        StringBuffer url = request.getRequestURL();
        String uri = request.getRequestURI();
        String ctx = request.getContextPath();
        String urlbase = url.substring(0, url.length() - uri.length() + ctx.length());

        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target(url + "/api/oxford/" + palabra);
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);

        Response apirestrespuesta = invocationBuilder.get();

        String apijson = apirestrespuesta.readEntity(String.class);
        System.out.println(apijson);
        JSONObject obj = new JSONObject(apijson);

        //ArrayList<String> definiciones = new ArrayList<>();
        String significado = obj.getJSONArray("results")                
                .getJSONObject(0)
                .getJSONArray("lexicalEntries")
                .getJSONObject(0)
                .getJSONArray("entries")
                .getJSONObject(0)
                .getJSONArray("senses")
                .getJSONObject(0)
                .getJSONArray("definitions")
                .getString(0);
        /*
        for (int i = 0; i < entries.length(); i++) {
            String definicion = entries.getJSONObject(i)
                    .getJSONArray("senses")
                    .getJSONObject(0)
                    .getJSONArray("definitions")
                    .getString(0);
            definiciones.add(definicion);
         */
        Diccionario palabraBD = new Diccionario();
        palabraBD.setDicPalabra(palabra);
        palabraBD.setDicSignificado(significado);
        
        DiccionarioDAO dao = new DiccionarioDAO();
        dao.create(palabraBD);
        request.setAttribute("palabraBD", palabraBD);
        
        request.getRequestDispatcher("respuesta.jsp").forward(request,response);   
         
    }

}

/**
 * Returns a short description of the servlet.
 *
 * @return a String containing servlet description
 */
/*
@Override
        public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
*/
