
package root.services;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.dto.Diccionariodto;

@Path("/oxford")
public class OxfordDiccionarioRest {
    
   @GET
   @Path("/{idbuscar}")
   @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo(@PathParam("idbuscar") String idbuscar){
       
      
        Client client = ClientBuilder.newClient();
        WebTarget webTarget = client.target("https://od-api.oxforddictionaries.com/api/v2/entries/es/" + idbuscar + "?fields=definitions&strictMatch=false");
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
        invocationBuilder.header("app_id", "b113b51f").header("app_key", "52100e3d8ca77023004c416615045f25");
        return invocationBuilder.get();
        
          /*
        System.out.println("REST diccionario:  apikey"+apikey);
        System.out.println("REST diccionario:  apiId"+apiId);
        System.out.println("REST diccionario :    palabra  "+idbuscar);
        
          
        Llamar a servicio de Oxford
       
        Client client = ClientBuilder.newClient();
        WebTarget myResource = client.target("http://DESKTOP-802N53F:8080/oxforddiccionario-rest-1.0-SNAPSHOT/api/oxford/" + palabra);
        
        pal = myResource.request(MediaType.APPLICATION_JSON).header("api-key", "KEYSEC0001918").header("api-id", "111111").get(root.dto.Diccionariodto.class);
        
        
        Diccionariodto pal = new Diccionariodto();
        pal.setDic_palabra(idbuscar);
        pal.setDic_significado("la palabra significa esto");
        */
                
       
        //return Response.ok(200).entity(pal).build();
    }
    
}
