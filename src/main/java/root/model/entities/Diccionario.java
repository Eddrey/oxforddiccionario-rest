/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.model.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Lewiss
 */
@Entity
@Table(name = "diccionario")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Diccionario.findAll", query = "SELECT d FROM Diccionario d"),
    @NamedQuery(name = "Diccionario.findByDicId", query = "SELECT d FROM Diccionario d WHERE d.dicId = :dicId"),
    @NamedQuery(name = "Diccionario.findByDicPalabra", query = "SELECT d FROM Diccionario d WHERE d.dicPalabra = :dicPalabra"),
    @NamedQuery(name = "Diccionario.findByDicSignificado", query = "SELECT d FROM Diccionario d WHERE d.dicSignificado = :dicSignificado")})
public class Diccionario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "dic_id")
    private Long dicId;
    @Size(max = 2147483647)
    @Column(name = "dic_palabra")
    private String dicPalabra;
    @Size(max = 2147483647)
    @Column(name = "dic_significado")
    private String dicSignificado;

    public Diccionario() {
    }

    public Diccionario(Long dicId) {
        this.dicId = dicId;
    }

    public Long getDicId() {
        return dicId;
    }

    public void setDicId(Long dicId) {
        this.dicId = dicId;
    }

    public String getDicPalabra() {
        return dicPalabra;
    }

    public void setDicPalabra(String dicPalabra) {
        this.dicPalabra = dicPalabra;
    }

    public String getDicSignificado() {
        return dicSignificado;
    }

    public void setDicSignificado(String dicSignificado) {
        this.dicSignificado = dicSignificado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (dicId != null ? dicId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Diccionario)) {
            return false;
        }
        Diccionario other = (Diccionario) object;
        if ((this.dicId == null && other.dicId != null) || (this.dicId != null && !this.dicId.equals(other.dicId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.model.entities.Diccionario[ dicId=" + dicId + " ]";
    }
    
}
