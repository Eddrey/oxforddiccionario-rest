
<%@page import="root.model.entities.Diccionario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Diccionario palabra = (Diccionario) request.getAttribute("palabraBD");
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <style>
            html, body {
                margin:0;
                padding:0;
                height:50%;
            }
            footer{
                height:50px;
                line-height:50px;
                background:black;
                color:white;
                text-align:center;
                position:fixed;
                bottom:0;
                left:0;
                width:100%
            } 
        </style>
        <title>Significado</title>
    </head>
    <body>
        <h1>El significado de la palabra es: <%=palabra.getDicSignificado()%></h1>

        <a href="index.jsp">Volver al inicio</a>
        <footer>
            <span>Taller de Aplicaciones Empresariales - Evaluación Final *** Eddrey Malebranche ***-<a href="https://bitbucket.org/Eddrey/oxforddiccionario-rest/src"> Enlace a Bitbucket </a></span>
        </footer> 
    </body>

</html>
